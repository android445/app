package com.naranya.mainactivity

import android.content.Intent
import android.content.Intent.getIntent
import android.os.Bundle
import android.provider.AlarmClock
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class HelloActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val message = intent.getStringExtra(AlarmClock.EXTRA_MESSAGE)
        val textView = findViewById<TextView>(R.id.textname).apply {
            text = message
        }
    }
}